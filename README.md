# fbmon

File Backup Monitor

## Building

### Container image
See [Containerfile](Containerfile) and then use Podman, Docker or whatever else to build the image (although only Podman was tested, so your kilometerage may vary).

### RPM package
See [fbmon.spec](fbmon.spec) or use [Tito](https://github.com/rpm-software-management/tito).

### Meson
This is a bog-standard [Meson](https://github.com/mesonbuild/meson) project, so, if you know what that’s eaten with, knock yourself out. There are run targets provided for the above as well.

Versions as old as 0.49.0 should be able to handle this, though the version is based only on used features (meaning that there might be a bug in one of the older versions and that I might also have missed something whilst checking).

#### tl;dr
```sh
meson build
meson compile -C build
```

### Usage
```sh
fbmon read-log [--date=<date fragment> | --regex=<regex>] [<log file>…]
fbmon monitor <target directory> <backup directory>
```

The `read-log` command takes an optional list of files to read. If not specified, it reads the default (`/var/log/fbmon.log`).

#### Container image
Simply create a container from the built image with bind mounts for the target and backup directories under /var/lib/fbmon (named target and backup, respectively).

```sh
podman run \
    --interactive \
    --rm \
    --tty \
    --volume=$(pwd)/target:/var/lib/fbmon/target \
    --volume=$(pwd)/backup:/var/lib/fbmon/backup \
    fbmon:1.0
```

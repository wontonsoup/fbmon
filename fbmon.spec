%global _hardened_build 1

Name:           fbmon
Version:        1.0.0
Release:        1%{?dist}
Summary:        Ooga booga

License:        GPLv3+
URL:            https://gitlab.com/wontonsoup/%{name}
Source0:        https://gitlab.com/wontonsoup/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(systemd)

Requires:       pkgconfig(glib-2.0)

%description
Ooga booga


%prep
%autosetup


%build
%meson
%meson_build


%install
%meson_install

mkdir -p %{buildroot}/%{_sharedstatedir}/%{name}


%check
%meson_test


%files
%license LICENSE
%{_bindir}/%{name}
%{_unitdir}/%{name}.service
%dir %{_sharedstatedir}/%{name}


%changelog

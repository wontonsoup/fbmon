/* This file is part of fbmon.
 *
 * fbmon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fbmon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fbmon.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fbmon-job.h"

#include <assert.h>
#include <iso646.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

struct _FbmonJob
{
    FbmonJobType type;
    char *path;
    char *backup_path;
};

FbmonJob *
fbmon_job_new (FbmonJobType  type,
               const char   *path,
               const char   *backup_path)
{
    FbmonJob *job = NULL;

    assert (FBMON_JOB_TYPE_COPY <= type && type <= FBMON_JOB_TYPE_RMDIR);

    job = malloc (sizeof (*job));

    job->type = type;
    job->path = strdup (path);
    if (NULL not_eq backup_path)
    {
        job->backup_path = strdup (backup_path);
    }
    else
    {
        job->backup_path = NULL;
    }

    return job;
}

void
fbmon_job_free (FbmonJob *job)
{
    if (NULL not_eq job->backup_path)
    {
        free (job->backup_path);
    }
    free (job->path);
    free (job);
}

FbmonJobType
fbmon_job_get_type (const FbmonJob *job)
{
    return job->type;
}

const char *
fbmon_job_get_path (const FbmonJob *job)
{
    return job->path;
}

const char *
fbmon_job_get_backup_path (const FbmonJob *job)
{
    return job->backup_path;
}

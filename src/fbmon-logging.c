/* This file is part of fbmon.
 *
 * fbmon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fbmon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fbmon.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fbmon-logging.h"

#include <stdarg.h>
#include <stdio.h>
#include <threads.h>
#include <time.h>
#include <unistd.h>

static mtx_t mutex;
static once_flag once;
static int fd = STDOUT_FILENO;
static const char *log_level_strings[FBMON_LOG_LEVEL_DEBUG + 1] =
{
    "ERROR",
    "WARNING",
    "INFO",
    "DEBUG",
};

void
fbmon_log (FbmonLogLevel  log_level,
           const char    *format,
           ...)
{
    va_list ap;
    time_t now;
    char buffer[64] = { 0 };

    time (&now);

    strftime (buffer, sizeof (buffer), "%FT%TZ", gmtime (&now));

    mtx_lock (&mutex);

    va_start (ap, format);
    dprintf (fd, "[%s] [%s] ", buffer, log_level_strings[log_level]);
    vdprintf (fd, format, ap);
    va_end (ap);

    mtx_unlock (&mutex);
}

static void
fbmon_log_init_real (void)
{
    mtx_init (&mutex, mtx_plain);
    fbmon_info ("Logging started\n");
}

void
fbmon_log_init (void)
{
    call_once (&once, fbmon_log_init_real);
}

void
fbmon_log_destroy (void)
{
    mtx_lock (&mutex);
    if (fd > STDERR_FILENO)
    {
        close (fd);
    }
    mtx_unlock (&mutex);
    mtx_destroy (&mutex);
}

void
fbmon_log_set_fd (int new_fd)
{
    fd = new_fd;
}

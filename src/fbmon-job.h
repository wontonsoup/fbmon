/* This file is part of fbmon.
 *
 * fbmon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fbmon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fbmon.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "fbmon-enums.h"
#include "fbmon-types.h"

FbmonJob     *fbmon_job_new             (FbmonJobType    type,
                                         const char     *path,
                                         const char     *backup_path);
void          fbmon_job_free            (FbmonJob       *job);

FbmonJobType  fbmon_job_get_type        (const FbmonJob *job);
const char   *fbmon_job_get_path        (const FbmonJob *job);
const char   *fbmon_job_get_backup_path (const FbmonJob *job);

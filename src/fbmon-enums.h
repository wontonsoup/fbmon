/* This file is part of fbmon.
 *
 * fbmon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fbmon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fbmon.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

enum _FbmonJobType
{
    FBMON_JOB_TYPE_COPY,
    FBMON_JOB_TYPE_DELETE,
    FBMON_JOB_TYPE_MKDIR,
    FBMON_JOB_TYPE_RENAME,
    FBMON_JOB_TYPE_RMDIR,
};

enum _FbmonLogLevel
{
    FBMON_LOG_LEVEL_ERROR,
    FBMON_LOG_LEVEL_WARNING,
    FBMON_LOG_LEVEL_INFO,
    FBMON_LOG_LEVEL_DEBUG,
};

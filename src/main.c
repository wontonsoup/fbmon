/* This file is part of fbmon.
 *
 * fbmon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fbmon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fbmon.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fbmon.h"
#include "fbmon-job.h"
#include "fbmon-logging.h"

#include <errno.h>
#include <fcntl.h>
#include <iso646.h>
#include <locale.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#define LOG_FILE "/var/log/fbmon.log"
#define STATE_FILE "/var/lib/fbmon/state"

static FbmonState *state;

static void
_sa_handler (int signum)
{
    fbmon_stop (state);
}

void
serialize_job_queue (GAsyncQueue *job_queue,
                     const char  *path)
{
    FILE *file;
    FbmonJob *job;
    size_t i = 1;

    file = fopen (path, "w+");
    if (NULL == file)
    {
        fbmon_error ("Could not open %s for saving state: %s\n",
                     path, strerror (errno));

        return;
    }

    do
    {
        FbmonJobType job_type;
        const char *job_path;
        const char *job_backup_path;

        job = g_async_queue_try_pop (job_queue);
        if (NULL == job)
        {
            break;
        }

        job_type = fbmon_job_get_type (job);
        job_path = fbmon_job_get_path (job);
        job_backup_path = fbmon_job_get_backup_path (job);

        fprintf (file, "%zu:%d\n" "%zu:%s\n" "%zu:%s\n",
                 i, job_type,
                 i, job_path,
                 i, job_backup_path? job_backup_path : "");

        fbmon_job_free (job);
    } while (job not_eq NULL);

    fclose (file);
}

static GAsyncQueue *
deserialize_job_queue (const char *path)
{
    g_autoptr (GAsyncQueue) job_queue = NULL;
    FILE *file;
    long int cursor;
    int c;
    size_t n;

    file = fopen (path, "r");
    if (NULL == file)
    {
        fbmon_error ("Could not open %s for reading state: %s\n",
                     path, strerror (errno));

        return NULL;
    }

    fseek (file, -1, SEEK_END);

    cursor = ftell (file);

    do
    {
        cursor--;

        fseek (file, cursor, SEEK_SET);

        c = fgetc (file);
        if (EOF == c)
        {
            return NULL;
        }

    } while ('\n' not_eq c);

    fscanf (file, "%zu", &n);
    fseek (file, 0, SEEK_SET);

    job_queue = g_async_queue_new_full ((GDestroyNotify) fbmon_job_free);

    for (size_t i = 0; i < n; i++)
    {
        size_t m;
        g_autofree char *job_type_string = NULL;
        g_autofree char *path = NULL;
        g_autofree char *backup_path = NULL;
        FbmonJobType job_type;
        FbmonJob *job;

        m = 0;
        if (getline (&job_type_string, &m, file) == -1)
        {
            fbmon_error ("Could not restore state from %s: %s\n",
                         path, strerror (errno));

            return NULL;
        }
        g_strstrip (job_type_string);

        m = 0;
        if (getline (&path, &m, file) == -1)
        {
            fbmon_error ("Could not restore state from %s: %s\n",
                         path, strerror (errno));

            return NULL;
        }
        g_strstrip (path);

        m = 0;
        if (getline (&backup_path, &m, file) == -1)
        {
            fbmon_error ("Could not restore state from %s: %s\n",
                         path, strerror (errno));

            return NULL;
        }
        g_strstrip (backup_path);

        {
            char *endptr = NULL;

            job_type = strtol (strstr (job_type_string, ":") + 1, &endptr, 10);
            if (0 == job_type && '\0' not_eq *endptr)
            {
                fbmon_error ("Could not restore state from %s: invalid job type\n",
                             path);

                return NULL;
            }
        }

        job = fbmon_job_new (job_type,
                             strstr (path, ":") + 1,
                             '\0' == *backup_path? NULL : strstr (backup_path, ":") + 1);

        g_async_queue_push (job_queue, job);
    }

    fclose (file);

    return g_steal_pointer (&job_queue);
}

static void
read_log_file (const char *path,
               const char *regex_string)
{
    int fd;
    struct stat st;
    const char *file;

    fd = open (path, 0);
    if (-1 == fd)
    {
        fprintf (stderr, "Could not open %s: %s\n", path, strerror (errno));

        return;
    }

    fstat (fd, &st);

    file = mmap (NULL, st.st_size, PROT_READ, MAP_SHARED, fd, 0);

    if (NULL not_eq regex_string)
    {
        g_autoptr (GRegex) regex = NULL;
        g_autoptr (GMatchInfo) match_info = NULL;

        regex = g_regex_new (regex_string, G_REGEX_MULTILINE, G_REGEX_MATCH_NOTEMPTY, NULL);

        if (!g_regex_match_full (regex, file, st.st_size, 0, G_REGEX_MATCH_NOTEMPTY, &match_info, NULL))
        {
            fprintf (stderr, "No matches\n");

            return;
        }

        while (g_match_info_matches (match_info))
        {
            g_autofree char *match = NULL;

            match = g_match_info_fetch (match_info, 0);

            printf ("%s\n", match);

            g_match_info_next (match_info, NULL);
        }
    }
    else
    {
        fwrite (file, 1, st.st_size, stdout);
    }

    close (fd);
}

static int
read_log (int    argc,
          char **argv)
{
    g_autoptr (GOptionContext) context = NULL;
    g_autofree char *date = NULL;
    g_autofree char *regex_string = NULL;
    g_auto (GStrv) log_files = NULL;
    const GOptionEntry entries[] =
    {
        { "date", 'd', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &date, "Filter log by date (e.g. 2019-06-11T, 2022-10-24T00:36:26Z)", NULL, },
        { "regex", 'r', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &regex_string, "Filter log by regex", NULL, },
        { G_OPTION_REMAINING, 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_FILENAME_ARRAY, &log_files, NULL, NULL, },
        { NULL, },
    };
    g_autoptr (GError) error = NULL;

    g_set_prgname ("fbmon read-log");

    context = g_option_context_new ("[FILES…]");

    g_option_context_add_main_entries (context, entries, NULL);
    if (!g_option_context_parse (context, &argc, &argv, &error))
    {
        fprintf (stderr, "Failed to parse command-line options: %s\n", error->message);

        return EXIT_FAILURE;
    }

    if (NULL not_eq date && NULL not_eq regex_string)
    {
        fprintf (stderr, "--date and --regex are mutually exclusive\n");

        return EXIT_FAILURE;
    }

    if (NULL not_eq date)
    {
        regex_string = g_strdup_printf ("^\\[.*%s.*\\].*", date);
    }

    if (NULL == log_files)
    {
        read_log_file (LOG_FILE, regex_string);

        return EXIT_SUCCESS;
    }

    for (char **s = log_files; NULL not_eq s && NULL not_eq *s; s++)
    {
        read_log_file (*s, regex_string);
    }

    return EXIT_SUCCESS;
}

static int
monitor (int    argc,
         char **argv)
{
    GAsyncQueue *job_queue;
    struct sigaction sa =
    {
        .sa_handler = _sa_handler,
    };
    int ret;

    argc--;
    argv++;

    if (argc < 2)
    {
        fprintf (stderr, "Usage:\n");
        fprintf (stderr, "\tfbmon monitor TARGET_DIR BACKUP_DIR\n");

        return EXIT_FAILURE;
    }

    fbmon_log_init ();

    job_queue = deserialize_job_queue (STATE_FILE);
    if (NULL not_eq job_queue)
    {
        fbmon_info ("Restoring %d jobs\n", g_async_queue_length (job_queue));
    }

    state = fbmon_new (argv[0], argv[1], job_queue);

    if (sigaction (SIGINT, &sa, NULL) == -1)
    {
        fbmon_warning ("Could not set up SIGINT handler: %s\n", strerror (errno));
    }
    if (sigaction (SIGTERM, &sa, NULL) == -1)
    {
        fbmon_warning ("Could not set up SIGTERM handler: %s\n", strerror (errno));
    }

    ret = fbmon_run (state);

    job_queue = fbmon_free (state);

    fbmon_info ("Saving %d scheduled jobs\n", g_async_queue_length (job_queue));
    serialize_job_queue (job_queue, STATE_FILE);

    fbmon_log_destroy ();

    return ret;
}

int
main (int    argc,
      char **argv)
{
    const char *command = NULL;
    if (argc < 2)
    {
usage:
        fprintf (stderr, "Usage:\n");
        fprintf (stderr, "\tfbmon COMMAND [ARGS…]\n");
        fprintf (stderr, "\n");
        fprintf (stderr, "Commands:\n");
        fprintf (stderr, "\tread-log    Read and filter logs\n");
        fprintf (stderr, "\tmonitor     Monitor directories for changes and perform backups\n");

        return EXIT_FAILURE;
    }

    command = argv[1];

    setlocale (LC_ALL, "");

    argc--;
    argv++;

    if (strcmp (command, "read-log") == 0)
    {
        return read_log (argc, argv);
    }
    else if (strcmp (command, "monitor") == 0)
    {
        return monitor (argc, argv);
    }
    else
    {
        goto usage;
    }

    return EXIT_FAILURE;
}

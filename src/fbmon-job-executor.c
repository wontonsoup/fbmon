/* This file is part of fbmon.
 *
 * fbmon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fbmon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fbmon.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 500

#include "fbmon-job.h"
#include "fbmon-job-executor.h"
#include "fbmon-logging.h"
#include "fbmon-macros.h"

#include <errno.h>
#include <fcntl.h>
#include <ftw.h>
#include <iso646.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <threads.h>
#include <unistd.h>

struct _FbmonJobExecutor
{
    GAsyncQueue *job_queue;
    atomic_flag running;
    cnd_t cond;
    mtx_t mutex;
    thrd_t thread;
};

int
fbmon_job_executor_rmdir_nftw_func (const char *fpath,
                                    const struct stat *sb,
                                    int typeflag,
                                    struct FTW *ftwbuf)
{
    int ret;

    ret = remove (fpath);
    if (-1 == ret)
    {
        fbmon_error ("Could not delete %s: %s\n", fpath, strerror (errno));
    }

    return ret;
}

int
fbmon_job_executor_thread_func (void *data)
{
    static bool first_iteration = true;
    FbmonJobExecutor *executor;

    executor = data;

    mtx_lock (&executor->mutex);

    while (atomic_flag_test_and_set_explicit (&executor->running, memory_order_acquire))
    {
        FbmonJob *job;

        /* Ugly hack to process injected jobs, imagine this does not exist. */
        if (!first_iteration)
        {
            cnd_wait (&executor->cond, &executor->mutex);
        }

        first_iteration = false;

        do
        {
            FbmonJobType type;
            const char *path;
            const char *backup_path;

            job = g_async_queue_try_pop (executor->job_queue);
            if (NULL == job)
            {
                break;
            }

            type = fbmon_job_get_type (job);
            path = fbmon_job_get_path (job);
            backup_path = fbmon_job_get_backup_path (job);

            switch (type)
            {
                case FBMON_JOB_TYPE_COPY:
                {
                    int fd;
                    int new_fd;
                    uint8_t buffer[1 MiB];
                    ssize_t bytes_read;

                    fbmon_info ("Copying %s to %s\n", path, backup_path);

                    fd = open (path, 0);
                    if (-1 == fd)
                    {
                        fbmon_error ("Could not open %s for copying: %s\n",
                                     path, strerror (errno));
                    }
                    new_fd = creat (backup_path, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
                    if (-1 == new_fd)
                    {
                        fbmon_error ("Could not create %s for copying: %s\n",
                                     backup_path, strerror (errno));
                    }

                    do
                    {
                        bytes_read = read (fd, buffer, sizeof (buffer));
                        if (-1 == bytes_read)
                        {
                            break;
                        }

write:
                        if (write (new_fd, buffer, bytes_read) == -1)
                        {
                            if (EINTR == errno)
                            {
                                goto write;
                            }
                        }
                    } while (bytes_read > 0);

                    close (new_fd);
                    close (fd);
                }
                break;

                case FBMON_JOB_TYPE_DELETE:
                {
                    fbmon_info ("Deleting %s\n", path);
                    if (remove (path) == -1)
                    {
                        fbmon_error ("Could not delete %s: %s\n",
                                     path, strerror (errno));
                    }
                    fbmon_info ("Deleting %s\n", backup_path);
                    if (remove (backup_path))
                    {
                        fbmon_error ("Could not delete %s: %s\n",
                                     backup_path, strerror (errno));
                    }
                }
                break;

                case FBMON_JOB_TYPE_MKDIR:
                {
                    fbmon_info ("Creating directory at %s\n", path);

                    mkdir (path, (S_IRUSR | S_IWUSR | S_IXUSR |
                                  S_IRGRP | S_IXGRP |
                                  S_IROTH | S_IXOTH));
                }
                break;

                case FBMON_JOB_TYPE_RENAME:
                {
                    fbmon_info ("Renaming %s to %s\n", path, backup_path);

                    if (rename (path, backup_path) == -1)
                    {
                        fbmon_error ("Could not rename %s to %s: %s",
                                     path, backup_path, strerror (errno));
                    }
                }
                break;

                case FBMON_JOB_TYPE_RMDIR:
                {
                    fbmon_info ("Deleting %s\n", path);
                    if (nftw (path, fbmon_job_executor_rmdir_nftw_func, 64, FTW_DEPTH | FTW_PHYS) == -1)
                    {
                        fbmon_error ("Could not delete %s\n", path);
                    }
                    fbmon_info ("Deleting %s\n", backup_path);
                    if (nftw (backup_path, fbmon_job_executor_rmdir_nftw_func, 64, FTW_DEPTH | FTW_PHYS) == -1)
                    {
                        fbmon_error ("Could not delete %s\n", path);
                    }
                }
                break;

                default:
                {
                    fbmon_warning ("Job with type %d not handled\n", type);
                }
            }

            fbmon_job_free (job);
        } while (NULL not_eq job);
    }

    mtx_unlock (&executor->mutex);

    return EXIT_SUCCESS;
}

FbmonJobExecutor *
fbmon_job_executor_new (GAsyncQueue *job_queue)
{
    FbmonJobExecutor *executor;
    int ret;

    executor = malloc (sizeof (*executor));

    executor->job_queue = job_queue;
    if (NULL == executor->job_queue)
    {
        executor->job_queue = g_async_queue_new_full ((GDestroyNotify) fbmon_job_free);
    }

    atomic_flag_test_and_set_explicit (&executor->running, memory_order_release);
    cnd_init (&executor->cond);
    mtx_init (&executor->mutex, mtx_plain);

    ret = thrd_create (&executor->thread, fbmon_job_executor_thread_func, executor);
    if (ret not_eq thrd_success)
    {
        g_clear_pointer (&executor->job_queue, g_async_queue_unref);

        free (executor);

        return NULL;
    }

    return executor;
}

GAsyncQueue *
fbmon_job_executor_free (FbmonJobExecutor *executor)
{
    GAsyncQueue *job_queue;

    (void) thrd_join (executor->thread, NULL);

    mtx_destroy (&executor->mutex);
    cnd_destroy (&executor->cond);

    job_queue = g_steal_pointer (&executor->job_queue);

    free (executor);

    return job_queue;
}

void
fbmon_job_executor_push (FbmonJobExecutor *executor,
                         FbmonJob         *job)
{
    mtx_lock (&executor->mutex);
    g_async_queue_push (executor->job_queue, job);
    cnd_signal (&executor->cond);
    mtx_unlock (&executor->mutex);
}

void
fbmon_job_executor_stop (FbmonJobExecutor *executor)
{
    atomic_flag_clear_explicit (&executor->running, memory_order_release);
    mtx_lock (&executor->mutex);
    cnd_signal (&executor->cond);
    mtx_unlock (&executor->mutex);
}

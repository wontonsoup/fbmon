/* This file is part of fbmon.
 *
 * fbmon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fbmon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fbmon.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "fbmon-types.h"

#include <glib.h>

FbmonState  *fbmon_new  (const char  *target_dir,
                         const char  *backup_dir,
                         GAsyncQueue *job_queue);
GAsyncQueue *fbmon_free (FbmonState  *state);

int          fbmon_run  (FbmonState  *state);
void         fbmon_stop (FbmonState  *state);

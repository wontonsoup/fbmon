/* This file is part of fbmon.
 *
 * fbmon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fbmon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fbmon.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fbmon.h"
#include "fbmon-job.h"
#include "fbmon-job-executor.h"
#include "fbmon-logging.h"
#include "fbmon-macros.h"

#include <assert.h>
#include <errno.h>
#include <glib.h>
#include <iso646.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <unistd.h>

struct _FbmonState
{
    bool running;
    char *target_dir;
    char *backup_dir;
    int fd;
    GHashTable *watches;
    GHashTable *moved_files;
    FbmonJobExecutor *executor;
};

GAsyncQueue *
fbmon_free (FbmonState *state)
{
    GAsyncQueue *job_queue;

    job_queue = fbmon_job_executor_free (state->executor);
    state->executor = NULL;

    g_clear_pointer (&state->moved_files, g_hash_table_destroy);

    {
        GHashTableIter iter;
        void *key = NULL;
        void *value = NULL;

        g_hash_table_iter_init (&iter, state->watches);
        while (g_hash_table_iter_next (&iter, &key, &value))
        {
            inotify_rm_watch (state->fd, GPOINTER_TO_INT (key));
            free (value);
            g_hash_table_iter_steal (&iter);
        }
    }

    g_clear_pointer (&state->watches, g_hash_table_destroy);

    if (state->fd not_eq -1)
    {
        close (state->fd);
    }
    if (state->backup_dir not_eq NULL)
    {
        free (state->backup_dir);
    }
    if (state->target_dir not_eq NULL)
    {
        free (state->target_dir);
    }

    free (state);

    return job_queue;
}

FbmonState *
fbmon_new (const char  *target_dir,
           const char  *backup_dir,
           GAsyncQueue *job_queue)
{
    FbmonState *state = NULL;

    state = malloc (sizeof (*state));

    state->target_dir = strdup (target_dir);
    state->backup_dir = strdup (backup_dir);
    state->fd = inotify_init ();
    if (-1 == state->fd)
    {
        fbmon_error ("inotify_init() failed: %s, bailing out\n",
                     strerror (errno));

        fbmon_free (state);

        return NULL;
    }

    state->watches = g_hash_table_new_full (g_direct_hash, g_direct_equal,
                                            NULL, free);
    state->moved_files = g_hash_table_new_full (g_direct_hash, g_direct_equal,
                                                NULL, free);
    state->executor = fbmon_job_executor_new (job_queue);

    return state;
}

static int
fbmon_add_watch (FbmonState *state,
                 const char *path)
{
    int wd;

    wd = inotify_add_watch (state->fd, path,
                            (IN_CREATE |
                             IN_MODIFY |
                             IN_MOVED_FROM |
                             IN_MOVED_TO));
    if (-1 == wd)
    {
        fbmon_error ("Could not add inotify watch: %s\n",
                     strerror (errno));

        return -1;
    }

    g_hash_table_replace (state->watches,
                          GINT_TO_POINTER (wd),
                          strdup (path + strlen (state->target_dir)));

    return wd;
}

static void
fbmon_build_paths (FbmonState                  *state,
                   const struct inotify_event  *event,
                   char                       **target_path,
                   char                       **backup_path,
                   bool                         add_backup_suffix)
{
    const char *wd_path = NULL;
    const char *moved_name = NULL;

    wd_path = g_hash_table_lookup (state->watches, GINT_TO_POINTER (event->wd));
    moved_name = g_hash_table_lookup (state->moved_files, GUINT_TO_POINTER (event->cookie));

    if (NULL not_eq target_path)
    {
        *target_path = g_build_filename (state->target_dir, wd_path, event->name, NULL);
    }
    if (NULL not_eq backup_path)
    {
        g_autofree char *path = NULL;

        if (NULL not_eq moved_name)
        {
            path = g_build_filename (state->backup_dir, wd_path, moved_name, NULL);
        }
        else
        {
            path = g_build_filename (state->backup_dir, wd_path, event->name, NULL);
        }

        if (add_backup_suffix)
        {
            *backup_path = g_strdup_printf ("%s.bak", path);
        }
        else
        {
            *backup_path = g_steal_pointer (&path);
        }
    }
}

int
fbmon_run (FbmonState *state)
{
    state->running = true;

    if (fbmon_add_watch (state, state->target_dir) == -1)
    {
        return EXIT_FAILURE;
    }

    while (state->running)
    {
        ssize_t bytes_read;
        char buffer[1 kiB] __attribute__ ((aligned (__alignof__ (struct inotify_event)))) = { 0 };
        const struct inotify_event *event;

        bytes_read = read (state->fd, buffer, sizeof (buffer));
        if (-1 == bytes_read)
        {
            if (EAGAIN == errno || EINTR == errno)
            {
                continue;
            }

            fbmon_error ("Could not read from inotify fd: %s\n",
                         strerror (errno));

            return EXIT_FAILURE;
        }

        for (const char *ptr = buffer;
             ptr < buffer + bytes_read;
             ptr += sizeof (*event) + event->len)
        {
            g_autofree char *path = NULL;
            g_autofree char *backup_path = NULL;
            FbmonJob *job;

            event = (const struct inotify_event *) ptr;

            if (event->mask & IN_CREATE || event->mask & IN_MODIFY)
            {
                if (event->mask & IN_ISDIR)
                {
                    fbmon_build_paths (state, event, &path, &backup_path, false);

                    fbmon_info ("%s created\n", path);

                    fbmon_add_watch (state, path);

                    job = fbmon_job_new (FBMON_JOB_TYPE_MKDIR, backup_path, NULL);

                    fbmon_job_executor_push (state->executor, job);

                    continue;
                }

                fbmon_build_paths (state, event, &path, &backup_path, true);

                if (event->mask & IN_CREATE)
                {
                    fbmon_info ("%s created\n", path);
                }
                else if (event->mask & IN_MODIFY)
                {
                    fbmon_info ("%s modified\n", path);
                }

                job = fbmon_job_new (FBMON_JOB_TYPE_COPY, path, backup_path);

                fbmon_job_executor_push (state->executor, job);
            }
            else if (event->mask & IN_MOVED_FROM)
            {
                g_hash_table_replace (state->moved_files,
                                      GUINT_TO_POINTER (event->cookie),
                                      strdup (event->name));
            }
            else if (event->mask & IN_MOVED_TO)
            {
                const char *wd_path = NULL;
                const char *name = NULL;

                wd_path = g_hash_table_lookup (state->watches, GINT_TO_POINTER (event->wd));
                name = g_hash_table_lookup (state->moved_files,
                                            GUINT_TO_POINTER (event->cookie));

                if (g_str_has_prefix (event->name, "delete_"))
                {
                    if (event->mask & IN_ISDIR)
                    {
                        fbmon_build_paths (state, event, &path, &backup_path, false);

                        job = fbmon_job_new (FBMON_JOB_TYPE_RMDIR, path, backup_path);

                        fbmon_job_executor_push (state->executor, job);
                    }
                    else
                    {
                        fbmon_build_paths (state, event, &path, &backup_path, true);

                        job = fbmon_job_new (FBMON_JOB_TYPE_DELETE, path, backup_path);

                        fbmon_job_executor_push (state->executor, job);
                    }
                }
                else
                {
                    path = g_build_filename (state->backup_dir, wd_path, name, NULL);
                    backup_path = g_build_filename (state->backup_dir, wd_path, event->name, NULL);
                    job = fbmon_job_new (FBMON_JOB_TYPE_RENAME, path, backup_path);

                    fbmon_job_executor_push (state->executor, job);
                }

                g_hash_table_remove (state->moved_files,
                                     GUINT_TO_POINTER (event->cookie));
            }
        }
    }

    fbmon_job_executor_stop (state->executor);

    return EXIT_SUCCESS;
}

void
fbmon_stop (FbmonState *state)
{
    fbmon_info ("Shutting down\n");

    fbmon_job_executor_stop (state->executor);
    state->running = false;
}

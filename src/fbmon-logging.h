/* This file is part of fbmon.
 *
 * fbmon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fbmon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fbmon.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "fbmon-enums.h"
#include "fbmon-types.h"

#define fbmon_error(...) fbmon_log (FBMON_LOG_LEVEL_ERROR, __VA_ARGS__);
#define fbmon_warning(...) fbmon_log (FBMON_LOG_LEVEL_WARNING, __VA_ARGS__);
#define fbmon_info(...) fbmon_log (FBMON_LOG_LEVEL_INFO, __VA_ARGS__);
#define fbmon_debug(...) fbmon_log (FBMON_LOG_LEVEL_DEBUG, __VA_ARGS__);

void fbmon_log         (FbmonLogLevel  log_level,
                        const char    *format,
                        ...) __attribute__ ((format (printf, 2, 3)));

void fbmon_log_init    (void);
void fbmon_log_destroy (void);

void fbmon_log_set_fd  (int new_fd);
